//
//  ViewController.m
//  RedditTest
//
//  Created by TheParkers on 6/18/14.
//  Copyright (c) 2014 TheParkers. All rights reserved.
//

#import "ViewController.h"
#import "RedditPost.h"

@interface ViewController ()
@property (strong, nonatomic) NSMutableArray* redditPostObjects;
@property (strong, nonatomic) IBOutlet UITableView *postsTable;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.postsTable setSeparatorInset:UIEdgeInsetsZero];
    self.postsTable.contentInset = UIEdgeInsetsMake(20.0f, 0.0f, 0.0f, 0.0f);
    
    self.redditPostObjects = [[NSMutableArray alloc] init];
    [self getRedditJSON];
}

-(void) getRedditJSON {
    NSString* URL = @"http://www.reddit.com/.json";
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:URL]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

                               NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                               if (error) {
                                   NSLog(@"Error loading%@", error);
                               }
                               else {
                                   NSLog(@"Array: %@", jsonDict);
                                   NSDictionary *data = [jsonDict objectForKey:@"data"];
                                   NSDictionary *chilrenDict = [data objectForKey:@"children"];
                                   [self createPostObjects:chilrenDict];
                               }
                               [self.postsTable reloadData];
                           }];

}

-(void) createPostObjects:(NSDictionary*) dict {
    //JSON stored as {data: {children:[ (post){data: {
    for (NSDictionary* postDict in dict) {
        NSDictionary* postTemp = [postDict objectForKey:@"data"];
        
        NSString* postTitle = [postTemp objectForKey:@"title"];
        NSString* postURLStr = [postTemp objectForKey:@"thumbnail"];
        
        NSURL* postURL = [NSURL URLWithString:postURLStr];
        NSNumber* nsfw = [postTemp objectForKey:@"over_18"];
        if (nsfw.boolValue) {
            continue;
        }
        __block RedditPost* post = [[RedditPost alloc] initWithTitle:postTitle url:postURL];
        [self.redditPostObjects addObject:post];
        
        NSURL* tempURL = [post getImageURL];
        if ([[tempURL absoluteString] isEqualToString:@""]) {
            continue;
        }
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:tempURL];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   if (error) {
                                       NSLog(@"problem with image: %@", error.localizedDescription);
                                   } else {
                                       UIImage* img = [UIImage imageWithData:data];
                                       [post setImageWithImage:img];
                                   }
                                   [self.postsTable reloadData];
                               }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView Delegate Methods


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.redditPostObjects count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* cellIdentifier = @"redditCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    RedditPost* postObj = [self.redditPostObjects objectAtIndex:indexPath.row];

    cell.textLabel.text = [postObj getTitle];
    [cell.textLabel setNumberOfLines:5];
    [cell.textLabel setFont:[UIFont systemFontOfSize:11]];
   
    NSString* imageURL = [[postObj getImageURL] absoluteString];
    if ([imageURL isEqualToString:@""]) {
        return cell;
    } else {
        cell.imageView.image = [postObj getPostImage];
    }

    return cell;
}


@end

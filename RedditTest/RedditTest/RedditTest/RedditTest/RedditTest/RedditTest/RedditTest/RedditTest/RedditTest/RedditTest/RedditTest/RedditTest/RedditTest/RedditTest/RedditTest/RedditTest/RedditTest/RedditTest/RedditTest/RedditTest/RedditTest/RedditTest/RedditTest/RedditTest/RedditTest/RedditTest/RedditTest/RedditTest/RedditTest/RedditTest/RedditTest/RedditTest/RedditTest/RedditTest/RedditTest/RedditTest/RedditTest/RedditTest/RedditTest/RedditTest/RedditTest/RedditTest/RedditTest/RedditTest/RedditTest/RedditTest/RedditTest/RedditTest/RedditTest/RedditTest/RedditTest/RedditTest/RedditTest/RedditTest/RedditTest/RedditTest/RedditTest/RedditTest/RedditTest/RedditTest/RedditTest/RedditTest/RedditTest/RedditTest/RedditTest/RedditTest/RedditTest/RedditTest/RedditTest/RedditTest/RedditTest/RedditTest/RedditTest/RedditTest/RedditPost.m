//
//  RedditPost.m
//  RedditTest
//
//  Created by TheParkers on 6/18/14.
//  Copyright (c) 2014 TheParkers. All rights reserved.
//

#import "RedditPost.h"

@interface RedditPost ()
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSURL* url;
@property (strong, nonatomic) UIImage* postImage;
@end

@implementation RedditPost

- (id)initWithTitle:(NSString*)title url:(NSURL*)url {
    self = [super init];
    if (self) {
        self.title = title;
        self.url = url;
        self.postImage = nil;
    }
    return self;
}

-(NSString*) getTitle {
    return self.title;
}

-(NSURL*) getImageURL {
    return self.url;
}

-(void) setImageWithImage:(UIImage*) img {
    self.postImage = img;
}

-(UIImage*) getPostImage {
    
    return self.postImage;
}
@end

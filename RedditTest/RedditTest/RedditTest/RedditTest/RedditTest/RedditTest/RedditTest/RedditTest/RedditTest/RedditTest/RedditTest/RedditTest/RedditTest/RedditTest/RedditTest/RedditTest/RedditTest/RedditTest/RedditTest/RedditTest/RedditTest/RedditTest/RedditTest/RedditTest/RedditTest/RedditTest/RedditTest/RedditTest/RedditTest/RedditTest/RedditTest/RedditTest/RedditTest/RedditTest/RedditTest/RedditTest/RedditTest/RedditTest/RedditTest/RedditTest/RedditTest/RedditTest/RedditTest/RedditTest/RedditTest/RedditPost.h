//
//  RedditPost.h
//  RedditTest
//
//  Created by TheParkers on 6/18/14.
//  Copyright (c) 2014 TheParkers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RedditPost : NSObject

- (id)initWithTitle:(NSString*)title url:(NSURL*)url;

-(NSString*) getTitle;
-(NSURL*) getImageURL;
-(UIImage*) getPostImage;
-(void) setImageWithImage:(UIImage*) img;

@end

//
//  AppDelegate.h
//  RedditTest
//
//  Created by TheParkers on 6/18/14.
//  Copyright (c) 2014 TheParkers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
